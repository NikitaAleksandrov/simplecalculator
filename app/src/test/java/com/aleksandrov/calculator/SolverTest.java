package com.aleksandrov.calculator;


import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class SolverTest extends TestCase {

    private Solver mSolver;

    @Before
    public void setUp() throws Exception {
        SolverFactory mSolverFactory = new SolverFactory();
        mSolver = mSolverFactory.getSolver(true);
    }

    @Test
    public void testSimpleOperations() throws Exception {
        assertEquals("4.0", mSolver.solve("2 + 2"));
        assertEquals("0.0", mSolver.solve("2 - 2"));
        assertEquals("4.0", mSolver.solve("2 * 2"));
        assertEquals("1.0", mSolver.solve("2 / 2"));
    }

    @Test
    public void testDifficult() throws Exception {
        assertEquals("8.0", mSolver.solve(" ( 2 + 2 ) + ( 2 + 2 ) "));
        assertEquals("0.0", mSolver.solve(" ( 2 - 2 ) + ( 2 - 2 ) "));
        assertEquals("8.0", mSolver.solve(" ( 2 * 2 ) + ( 2 * 2 ) "));
        assertEquals("2.0", mSolver.solve(" ( 2 / 2 ) + ( 2 / 2 ) "));

        assertEquals("16.0", mSolver.solve(" ( ( 2 + 2 ) + ( 2 + 2 ) ) * 2 "));
        assertEquals("0.0", mSolver.solve(" ( ( 2 - 2 ) + ( 2 - 2 ) / 2 ) "));
        assertEquals("6.0", mSolver.solve(" ( ( 2 * 2 ) + ( 2 * 2 ) - 2 ) "));
        assertEquals("4.0", mSolver.solve(" ( ( 2 / 2 ) + ( 2 / 2 ) + 2 ) "));

        assertEquals("-4.0", mSolver.solve(" ( 2 - 4 ) * 2 "));
        assertEquals("16.0", mSolver.solve(" ( ( 2 - 4 ) * 2 ) * ( ( 2 - 4 ) * 2 ) "));
    }

    @Test
    public void testFails() throws Exception {
        assertEquals("Пустая строка", mSolver.solve(""));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" ( "));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" (  ( "));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" ) "));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" )  ) "));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" ( + 2 ( "));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" ) + 2 ) "));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" ( 2 + 2 ) ) "));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" ( ( 2 + 2 ) "));
        assertEquals("Ошибка во входном выражении. Стек не пуст", mSolver.solve(" ( 2 + 2 ) 2"));
        assertEquals("Извлечение квадратного корня из отрицательного числа", mSolver.solve(" sqrt ( - 4 ) "));
        assertEquals("Ошибка во входном выражении", mSolver.solve("4...............4"));
        assertEquals("Ошибка во входном выражении", mSolver.solve("4.4.5.4"));
        assertEquals("Ошибка во входном выражении", mSolver.solve("."));
    }

    @Test
    public void testSin() throws Exception {
        assertEquals("Ошибка во входном выражении", mSolver.solve(" sin ( "));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" sin ( ) "));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" sin ( ) ) "));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" sin ( ) ) "));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" sin ( ) ( "));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" sin ( ) * ( "));
        assertEquals("0.8415", mSolver.solve(" sin ( 1 ) "));
        assertEquals("1.8415", mSolver.solve(" sin ( 1 ) + 1"));
        assertEquals("2.8415", mSolver.solve(" 1 + sin ( 1 ) + 1"));
        assertEquals("5.683", mSolver.solve(" ( 1 + sin ( 1 ) + 1 ) * 2"));
        assertEquals("5.683", mSolver.solve(" ( 1 + sin ( 1 + ( 2 - 2 ) ) + 1 ) * 2"));
    }

    @Test
    public void testCos() throws Exception {
        assertEquals("Ошибка во входном выражении", mSolver.solve(" cos ( "));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" cos ( ) "));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" cos ( ) ) "));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" cos ( ) ) "));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" cos ( ) ( "));
        assertEquals("Ошибка во входном выражении", mSolver.solve(" cos ( ) * ( "));
        assertEquals("0.5403", mSolver.solve(" cos ( 1 ) "));
        assertEquals("1.5403", mSolver.solve(" cos ( 1 ) + 1"));
        assertEquals("2.0", mSolver.solve(" ( ( 1 + cos ( 1 ) ) - 0.5403 ) + 1"));
        assertEquals("4.0", mSolver.solve(" ( ( 1 + cos ( 1 ) - 0.5403 ) + 1 ) * 2"));
        assertEquals("4.9193999999999996", mSolver.solve(" ( ( 1 + cos ( 2 - 2 ) - 0.5403 ) + 1 ) * 2"));
    }

    @Test
    public void testSqrt() throws Exception {
        assertEquals("2.0", mSolver.solve(" sqrt ( 4 ) "));
        assertEquals("0.0", mSolver.solve(" sqrt ( - 4 + 4 ) "));
        assertEquals("0.0", mSolver.solve(" 2 + sqrt ( 4 - 4 ) - 2 "));
        assertEquals("0.0", mSolver.solve(" sqrt ( ( 4 - 4 ) + ( - 4 + 4 ) ) "));
        assertEquals("Извлечение квадратного корня из отрицательного числа", mSolver.solve(" sqrt ( ( 4 - 4 ) + ( 4 - 4 ) - 1 ) "));
    }

    @Test
    public void testPow() throws Exception {
        assertEquals("4.0", mSolver.solve(" pow ( 2 ) "));
        assertEquals("16.0", mSolver.solve(" pow ( 2 + 2 ) "));
        assertEquals("0.0", mSolver.solve(" pow ( 2 - 2 ) "));
        assertEquals("16.0", mSolver.solve(" pow ( 0 - 2 - 2 ) "));
        assertEquals("16.0", mSolver.solve(" 2 + pow ( 0 - 2 - 2 ) - 2 "));
    }
}
