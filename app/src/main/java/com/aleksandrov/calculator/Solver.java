package com.aleksandrov.calculator;

public interface Solver {
    public String solve(String expression);
}
