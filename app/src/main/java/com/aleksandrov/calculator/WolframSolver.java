package com.aleksandrov.calculator;

import android.util.Log;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class WolframSolver implements Solver {

    /*
    APP NAME: AndroidCalculator

    APPID: WXJXWP-56LRU8TPV7
     */
    public final static String APPID = "WXJXWP-56LRU8TPV7";

    /**
     * <p>Решает входное выражение с помощью WolframAlpha</p>
     * @param expression Входная строка
     * @return Результат
     */
    @Override
    public String solve(String expression) {

        //Запрос
        String xmlFromWolfram = sendRequestToWolfram(expression);
        Log.d("XML", xmlFromWolfram);

        //Разбор приходящей строки
        String result = getSolveFromXML(xmlFromWolfram);
        if (!result.equals("")) {
            return result;
        } else {
            return "Ошибка во входном выражении";
        }
    }

    /**
     * <p>Разбор xml ответа, который пришёл с сервера</p>
     * @param xmlFromWolfram xml ответ с сервера
     * @return Результат
     */
    public String getSolveFromXML(String xmlFromWolfram) {

        String result = "";
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xmlFromWolfram));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("pod");
            for (int i = 0; i < nodes.getLength(); i++) {
                Element element = (Element) nodes.item(i);
                Log.d("attributes", element.getAttribute("title"));
                if (element.getAttribute("title").equals("Result") ||
                    element.getAttribute("title").equals("Exact result") ||
                    element.getAttribute("title").equals("Decimal form") ||
                    element.getAttribute("title").equals("Decimal approximation")) {

                    Log.d("subpods", "GOT_IT");
                    NodeList subpods = element.getElementsByTagName("subpod");
                    for (int j = 0; j < subpods.getLength(); j++) {
                        Element element2 = (Element) subpods.item(j);
                        NodeList plainText = element2.getElementsByTagName("plaintext");
                        Element line = (Element) plainText.item(0);
                        Log.d("Results", "plaintext: " + getCharacterDataFromElement(line));
                        result = getCharacterDataFromElement(line);
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * <p>Получает значение из xml узла</p>
     * @param e XML узел
     * @return Результат в xml узле
     */
    public static String getCharacterDataFromElement(Element e) {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            CharacterData cd = (CharacterData) child;
            return cd.getData();
        }
        return "?";
    }

    /**
     * Отправка запроса на Wolfram Alpha
     * @param expression Входная строка с выражением
     * @return xml результат, который пришёл с Wolfram Alpha
     */
    public String sendRequestToWolfram(String expression) {
        try {
            expression = URLEncoder.encode(expression, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String link = "http://api.wolframalpha.com/v2/query?appid=" + APPID + "&input=" + expression + "&format=plaintext";
        Log.d("LINK", link);
        String result = "";
        try {
            URL url = new URL(link);
            try {
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("GET");

                int responseCode = connection.getResponseCode();

                Log.d("RESPONSE_CODE", "" + responseCode);

                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                result = response.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
