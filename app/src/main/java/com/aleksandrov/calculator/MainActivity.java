package com.aleksandrov.calculator;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button calcButtonOne;
    Button calcButtonTwo;
    Button calcButtonThree;
    Button calcButtonFour;
    Button calcButtonFive;
    Button calcButtonSix;
    Button calcButtonSeven;
    Button calcButtonEight;
    Button calcButtonNine;
    Button calcButtonZero;

    Button calcButtonAdd;
    Button calcButtonSub;
    Button calcButtonMul;
    Button calcButtonDiv;

    Button calcButtonClear;
    Button calcButtonResult;
    Button calcButtonDot;

    Button calcButtonLeftBr;
    Button calcButtonRightBr;

    Button calcButtonCos;
    Button calcButtonSin;
    Button calcButtonSqrt;
    Button calcButtonSqr;

    TextView calcResultView;

    SolverFactory mFactory = new SolverFactory();
    Solver mSolver = null;

    WolframAlphaTask mTask;

    private ProgressDialog mProgressDialog;
    private Context mContext;

    boolean mNewExpression = false;
    boolean mIsLocal = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        mContext = this;

        prepareCalculator();
        //prepareCalculatorOperaions();

        //Фабричный метод можно использовать для различных Solver-ов
        //Отдельный для Вольфрама и локальных вычислений

    }

    @Override
    protected  void onDestroy() {
        super.onDestroy();
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            mIsLocal = !mIsLocal;
            if (mIsLocal) {
                Toast.makeText(mContext, "Локальное решение", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mContext, "Решение через Вольфрам", Toast.LENGTH_SHORT).show();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void prepareCalculator() {

        calcButtonOne = (Button) findViewById(R.id.calc_button_1);
        calcButtonOne.setOnClickListener(this);

        calcButtonTwo = (Button) findViewById(R.id.calc_button_2);
        calcButtonTwo.setOnClickListener(this);

        calcButtonThree = (Button) findViewById(R.id.calc_button_3);
        calcButtonThree.setOnClickListener(this);

        calcButtonFour = (Button) findViewById(R.id.calc_button_4);
        calcButtonFour.setOnClickListener(this);

        calcButtonFive = (Button) findViewById(R.id.calc_button_5);
        calcButtonFive.setOnClickListener(this);

        calcButtonSix = (Button) findViewById(R.id.calc_button_6);
        calcButtonSix.setOnClickListener(this);

        calcButtonSeven = (Button) findViewById(R.id.calc_button_7);
        calcButtonSeven.setOnClickListener(this);

        calcButtonEight = (Button) findViewById(R.id.calc_button_8);
        calcButtonEight.setOnClickListener(this);

        calcButtonNine = (Button) findViewById(R.id.calc_button_9);
        calcButtonNine.setOnClickListener(this);

        calcButtonZero = (Button) findViewById(R.id.calc_button_0);
        calcButtonZero.setOnClickListener(this);

        calcButtonAdd = (Button) findViewById(R.id.calc_button_add);
        calcButtonAdd.setOnClickListener(this);

        calcButtonSub = (Button) findViewById(R.id.calc_button_sub);
        calcButtonSub.setOnClickListener(this);

        calcButtonDiv = (Button) findViewById(R.id.calc_button_div);
        calcButtonDiv.setOnClickListener(this);

        calcButtonMul = (Button) findViewById(R.id.calc_button_mul);
        calcButtonMul.setOnClickListener(this);

        calcButtonClear = (Button) findViewById(R.id.calc_button_clear);
        calcButtonClear.setOnClickListener(this);

        calcButtonDot = (Button) findViewById(R.id.calc_button_dot);
        calcButtonDot.setOnClickListener(this);

        calcButtonResult = (Button) findViewById(R.id.calc_button_result);
        calcButtonResult.setOnClickListener(this);

        calcButtonLeftBr = (Button) findViewById(R.id.calc_button_left_br);
        calcButtonLeftBr.setOnClickListener(this);

        calcButtonRightBr = (Button) findViewById(R.id.calc_button_right_br);
        calcButtonRightBr.setOnClickListener(this);

        calcButtonSin = (Button) findViewById(R.id.calc_button_sin);
        calcButtonSin.setOnClickListener(this);

        calcButtonCos = (Button) findViewById(R.id.calc_button_cos);
        calcButtonCos.setOnClickListener(this);

        calcButtonSqr = (Button) findViewById(R.id.calc_button_pow);
        calcButtonSqr.setOnClickListener(this);

        calcButtonSqrt = (Button) findViewById(R.id.calc_button_sqrt);
        calcButtonSqrt.setOnClickListener(this);

        /*Возможно эту штуку надо отдельно объявлять*/
        calcResultView = (TextView) findViewById(R.id.calc_result_view);
    }

    @Override
    public void onClick(View v) {

        if (mNewExpression) {
            calcResultView.setText("");
            mNewExpression = false;
        }

        switch(v.getId()) {
            case R.id.calc_button_1:
                calcResultView.setText(calcResultView.getText() + "1");
                break;
            case R.id.calc_button_2:
                calcResultView.setText(calcResultView.getText() + "2");
                break;
            case R.id.calc_button_3:
                calcResultView.setText(calcResultView.getText() + "3");
                break;
            case R.id.calc_button_4:
                calcResultView.setText(calcResultView.getText() + "4");
                break;
            case R.id.calc_button_5:
                calcResultView.setText(calcResultView.getText() + "5");
                break;
            case R.id.calc_button_6:
                calcResultView.setText(calcResultView.getText() + "6");
                break;
            case R.id.calc_button_7:
                calcResultView.setText(calcResultView.getText() + "7");
                break;
            case R.id.calc_button_8:
                calcResultView.setText(calcResultView.getText() + "8");
                break;
            case R.id.calc_button_9:
                calcResultView.setText(calcResultView.getText() + "9");
                break;
            case R.id.calc_button_0:
                calcResultView.setText(calcResultView.getText() + "0");
                break;
            case R.id.calc_button_add:
                calcResultView.setText(calcResultView.getText() + " + ");
                break;
            case R.id.calc_button_sub:
                calcResultView.setText(calcResultView.getText() + " - ");
                break;
            case R.id.calc_button_div:
                calcResultView.setText(calcResultView.getText() + " / ");
                break;
            case R.id.calc_button_mul:
                calcResultView.setText(calcResultView.getText() + " * ");
                break;
            case R.id.calc_button_clear:
                calcResultView.setText(removeLastChar(calcResultView.getText().toString()));
                break;
            case R.id.calc_button_dot:
                calcResultView.setText(calcResultView.getText() + ".");
                break;
            case R.id.calc_button_left_br:
                calcResultView.setText(calcResultView.getText() + " ( ");
                break;
            case R.id.calc_button_right_br:
                calcResultView.setText(calcResultView.getText() + " ) ");
                break;
            case R.id.calc_button_sin:
                calcResultView.setText(calcResultView.getText() + "sin ( ");
                break;
            case R.id.calc_button_cos:
                calcResultView.setText(calcResultView.getText() + "cos ( ");
                break;
            case R.id.calc_button_pow:
                if (mIsLocal) {
                    calcResultView.setText(calcResultView.getText() + "pow ( ");
                } else {
                    calcResultView.setText(calcResultView.getText() + " ^ ");
                }
                break;
            case R.id.calc_button_sqrt:
                calcResultView.setText(calcResultView.getText() + "sqrt ( ");
                break;
            case R.id.calc_button_result:
                //Запустить решатель
                mSolver = mFactory.getSolver(mIsLocal);
                if (mIsLocal) {
                    calcResultView.setText(mSolver.solve(calcResultView.getText().toString()));
                } else {
                    if (checkNetworkConnection()) {
                        mTask = new WolframAlphaTask();
                        mTask.execute(calcResultView.getText().toString());
                    } else {
                        calcResultView.setText("Вольфрам сейчас недоступен");
                    }
                }
                mNewExpression = true;
                break;
        }
    }

    public String removeLastChar(String s) {
        if (s == null || s.length() == 0) {
            return s;
        }
        return s.substring(0, s.length() - 1);
    }

    class WolframAlphaTask extends AsyncTask<String, Void, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("WOLFRAM", "Start");
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setMessage("Подождите");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return mSolver.solve(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            calcResultView.setText(result);
            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }
        }
    }

    public boolean checkNetworkConnection() {
        boolean haveConnectedWiFi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();

        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI")) {
                if (ni.isConnected())
                    haveConnectedWiFi = true;
            }
            if (ni.getTypeName().equalsIgnoreCase("MOBILE")) {
                if (ni.isConnected())
                    haveConnectedMobile = true;
            }
        }
        return haveConnectedMobile || haveConnectedWiFi;
    }
}
