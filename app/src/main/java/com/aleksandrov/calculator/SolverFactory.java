package com.aleksandrov.calculator;

public class SolverFactory {

    public Solver getSolver(boolean isLocal) {
        if (isLocal) {
            return new LocalSolver();
        } else {
            return new WolframSolver();
        }
    }
}
