package com.aleksandrov.calculator;

import android.util.Log;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.StringTokenizer;

public class LocalSolver implements Solver {

    //Для начала определим знаки операторов
    public static final Map<String, Integer> OPERATIONS;

    static {
        OPERATIONS = new HashMap<String, Integer>();
        OPERATIONS.put("sin", 1);
        OPERATIONS.put("cos", 1);
        OPERATIONS.put("pow", 1);
        OPERATIONS.put("sqrt", 1);
        OPERATIONS.put("*", 1);
        OPERATIONS.put("/", 1);
        OPERATIONS.put("+", 2);
        OPERATIONS.put("-", 2);
    }

    //Теперь определим скобки
    public static final Set<String> OPERATING_SYMBOLS;

    static {
        OPERATING_SYMBOLS = new HashSet<String>(OPERATIONS.keySet());
        OPERATING_SYMBOLS.add("(");
        OPERATING_SYMBOLS.add(")");
    }

    /**
     * <p>Решение входного выражения</p>
     * @param expression Входное выражение
     * @return результат либо сообщение об ошибке в выражении
     */
    @Override
    public String solve(String expression) {
        if (!expression.equals("")) {
            //Разбиение входной строки на отдельные элементы
            List<String> preparedExpression = prepareExpression(expression);
            //Преобразование её в обратную польскую нотацию
            List<String> convertedExpression = convertToRPN(preparedExpression);
            //Подсчёт результата
            return calculateResult(convertedExpression);
        } else {
            return "Пустая строка";
        }
    }

    /**
     * <p>Подготовка входного выражения к конвертации в обратную польскую запись</p>
     * @param expression входное выражение
     * @return входное выражение подготовленное к конвертации
     */
    public List<String> prepareExpression(String expression) {
        String[] preparedExpression = expression.replace("  ", " ").split(" ");
        List<String> resultExpression = new ArrayList<String>();
        for (String expressionUnit : preparedExpression) {
            if (!expressionUnit.equals("")) {
                resultExpression.add(expressionUnit);
            }
        }
        return resultExpression;
    }

    /**
     * <p>Приведение входного выражения к обратной польской записи</p>
     * @param inputExpression Входное выражение, которое было разделено по ячейкам массива
     * @return Выражение приведённое к виду обратной польской нотации
     */
    public List<String> convertToRPN(List<String> inputExpression) {

        //Необходимые модели данных
        List<String> resultExpression = new ArrayList<String>();
        Stack<String> stack = new Stack<String>();

        //Начинаем алгоритм
        //Всё просто. Проверяем ячейки массива. Если в ячейке число,
        //то тут же отправляем его в выходной список. Если в ячейке
        //знак действия или скобка, то проверяем по таблице и потом либо
        //отправляем последний элемент из стека в выходной список
        //и добавляем этот элемент в стек либо отправляем в стек элемент сразу
        for (String expression : inputExpression) {
            Log.d("ALG", expression);
            Log.d("ALG", "" + expression.length());
            if (!OPERATING_SYMBOLS.contains(expression)) {
                resultExpression.add(expression);
            } else {
                //Узнаём какой перед нами символ
                switch (expression) {
                    case "(":
                        stack.push(expression);
                        break;
                    case ")":
                        if (stack.empty()) {
                            try {
                                throw new EmptyStackException();
                            } catch (EmptyStackException e) {
                                Log.d("EmptyStackException", "Есть правая скобка, но стек пуст");
                                resultExpression.clear();
                                return resultExpression;
                            }
                        } else {
                            while (!stack.peek().equals("(")) {
                                resultExpression.add(stack.pop());
                                if (stack.empty()) {
                                    try {
                                        throw new EmptyStackException();
                                    } catch (EmptyStackException e) {
                                        Log.d("EmptyStackException", "Есть правая скобка, но стек пуст");
                                        resultExpression.clear();
                                        return resultExpression;
                                    }
                                }
                            }
                            stack.pop();
                            break;
                        }
                    default:
                        while (!stack.isEmpty() && !stack.peek().equals("(") &&
                                (OPERATIONS.get(expression) >= OPERATIONS.get(stack.peek()))) {
                            resultExpression.add(stack.pop());
                        }
                        stack.push(expression);
                        break;
                }
            }
        }
        //Преобразование выходного списка к выходной строке
        while (!stack.empty()) {
            resultExpression.add(stack.pop());
        }
        return resultExpression;
    }

    /**
     * <p>Вычисляет результат</p>
     * @param expression Выражение приведённое к виду обратной польской нотации
     * @return Выходная строка с результатом либо с сообщением об ошибке
     */
    public String calculateResult(List<String> expression) {
        for (int i = 0; i < expression.size(); i++) {
            Log.d("result", expression.get(i));
        }
        if (!expression.isEmpty()
                && !expression.contains("(")
                && !expression.contains(")")
                && !expression.contains(".")) {
            Stack<Double> stack = new Stack<Double>();
            for (int i = 0; i < expression.size(); i++) {
                String token = expression.get(i);
                Log.d("token", token);
                //Операнд
                if (!OPERATIONS.keySet().contains(token)) {
                    if (isDouble(token)) {
                        stack.push(Double.valueOf(token));
                    } else {
                        return "Ошибка во входном выражении";
                    }
                } else {
                    if (stack.isEmpty()) {
                        return "Ошибка во входном выражении";
                    }
                    Double operand2 = stack.pop();
                    Double operand1 = stack.empty() ? 0.0 : stack.pop();
                    Log.d("OPERAND1", operand1.toString());
                    Log.d("OPERAND2", operand2.toString());
                    switch (token) {
                        case "*":
                            stack.push(operand1 * operand2);
                            break;
                        case "/":
                            if (!operand2.equals(0.0)) {
                                stack.push(operand1 / operand2);
                            } else {
                                return "Деление на ноль";
                            }
                            break;
                        case "sin":
                            if (!operand1.equals(0.0)) {
                                stack.push(operand1);
                            }
                            stack.push(new BigDecimal(Math.sin(operand2)).setScale(4, RoundingMode.HALF_UP).doubleValue());
                            break;
                        case "cos":
                            if (!operand1.equals(0.0)) {
                                stack.push(operand1);
                            }
                            stack.push(new BigDecimal(Math.cos(operand2)).setScale(4, RoundingMode.HALF_UP).doubleValue());
                            break;
                        case "sqrt":
                            if (!operand1.equals(0.0)) {
                                stack.push(operand1);
                            }
                            if (!(operand2 < 0)) {
                                stack.push(new BigDecimal(Math.sqrt(operand2)).setScale(4, RoundingMode.HALF_UP).doubleValue());
                            } else {
                                return "Извлечение квадратного корня из отрицательного числа";
                            }
                            break;
                        case "pow":
                            if (!operand1.equals(0.0)) {
                                stack.push(operand1);
                            }
                            stack.push(Math.pow(operand2, 2));
                            break;
                        case "+":
                            stack.push(operand1 + operand2);
                            break;
                        case "-":
                            stack.push(operand1 - operand2);
                            break;
                    }
                }
            }
            if (stack.size() != 1) {
                //Из-за унарных операций возможны случаи, когда в стеке остаётся два операнда
                // 0 и число, возможно их просто надо сложить друг с другом и всё
                //Есть случай когда (2+2)2 по идее это должно быть 8 но пока это ошибка
                return "Ошибка во входном выражении. Стек не пуст";
            }
            return stack.pop().toString();
        } else {
            return "Ошибка во входном выражении";
        }
    }

    /**
     * <p>Проверяет является ли входная строка Double числом</p>
     * @param str входная строка в которой может содержаться число
     * @return Возвращается true если в строке число, false если нет
     */
    public boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}